archetype (adl_version=1.4; uid=08448afd-d202-4a07-ba68-c838d561519b)
	openEHR-EHR-CLUSTER.mnehr_allergy_info_source.v0

concept
	[at0000]

language
	original_language = <[ISO_639-1::en]>

description
	original_author = <
		["date"] = <"2019-04-30">
	>
	lifecycle_state = <"unmanaged">
	details = <
		["en"] = <
			language = <[ISO_639-1::en]>
		>
	>
	other_details = <
		["licence"] = <"This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.">
		["custodian_organisation"] = <"openEHR Foundation">
		["original_namespace"] = <"org.openehr">
		["original_publisher"] = <"openEHR Foundation">
		["custodian_namespace"] = <"org.openehr">
		["MD5-CAM-1.0.1"] = <"fd80b16a1eaea40058ff0573e86dd201">
		["build_uid"] = <"ad093ad7-1037-32ad-a41a-e42e96624f2e">
	>

definition
	CLUSTER[at0000] matches {    -- Allergy information source
		items cardinality matches {1..*; unordered} matches {
			ELEMENT[at0001] occurrences matches {0..1} matches {    -- Information source
				value matches {
					DV_CODED_TEXT matches {
						defining_code matches {
							[local::
							at0002,    -- GP
							at0003,    -- Hospital staff
							at0004,    -- Item worn by patient
							at0005,    -- Existing medical records
							at0006,    -- Patient
							at0007,    -- Relative, friend or carer
							at0008]    -- Physical examination
						}
					}
					DV_TEXT matches {*}
				}
			}
		}
	}

ontology
	term_definitions = <
		["en"] = <
			items = <
				["at0000"] = <
					text = <"Allergy information source">
					description = <"MNEHR_allergy_info_source">
				>
				["at0001"] = <
					text = <"Information source">
					description = <"What was the information source for this allergy record.">
				>
				["at0002"] = <
					text = <"GP">
					description = <"The patient's GP.">
				>
				["at0003"] = <
					text = <"Hospital staff">
					description = <"Hospital staff provided the information.">
				>
				["at0004"] = <
					text = <"Item worn by patient">
					description = <"An alert mechanism or device worn by the patient.">
				>
				["at0005"] = <
					text = <"Existing medical records">
					description = <"The information was recorded in existing medical records">
				>
				["at0006"] = <
					text = <"Patient">
					description = <"The history given by the patient.">
				>
				["at0007"] = <
					text = <"Relative, friend or carer">
					description = <"The history was given by a friend, relative or carer.">
				>
				["at0008"] = <
					text = <"Physical examination">
					description = <"The reaction was identified by physical examination.">
				>
			>
		>
	>
